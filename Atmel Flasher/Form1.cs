﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Atmel_Flasher;

namespace Atmel_Flasher
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int size = -1;
            DialogResult result = openELF.ShowDialog(); // Show the dialog.
            if (result == DialogResult.OK) // Test result.
            {
                string file = openELF.FileName;
                textELF_HEX.Text = file;
                //MessageBox.Show(file);
            }
            Console.WriteLine(size); // <-- Shows file size in debugging mode.
            Console.WriteLine(result); // <-- For debugging use.
        }

        private void button_flash_Click(object sender, EventArgs e)
        {
            string output;
            string programmer = comboBox2.Text;
            string programming_mode = comboBox3.Text;
            string target_device = comboBox1.Text;
            string operation_mode = comboBox4.Text;
            string file_path = textELF_HEX.Text;
            string current_dir = Directory.GetCurrentDirectory();
            output = Program.flash(programmer, programming_mode, target_device, operation_mode, 
                                    file_path, current_dir, checkBoxErase.Checked, checkBoxVerify.Checked);
            textBox1.Text = output;
        }


        private void comboBox4_TextChanged(object sender, EventArgs e)
        {
            string compare = comboBox4.Text;
            string test = "program";
            if (compare.Equals(test))
            {
                textELF_HEX.Visible = true;
                button1.Visible = true;
                label1.Visible = true;
                checkBoxErase.Visible = true;
                checkBoxVerify.Visible = true;
            }
            else
            {
                textELF_HEX.Visible = false;
                button1.Visible = false;
                label1.Visible = false;
                textELF_HEX.Text = null;
                checkBoxErase.Visible = false;
                checkBoxVerify.Visible = false;
            }
        }
    }
}
