﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Atmel_Flasher
{
    static class Program
    {
        public static string flash(string programmer,
                            string programming_mode,
                            string target_device,
                            string operation_mode,
                            string file_path,
                            string current_dir, bool erase, bool verify)
        {
            string output;
            string mode_arg;
            string erase_arg = null;
            string verify_arg = null;
            string change_dir_arg = "/C cd \"" + current_dir + "\" & atprogram -v -t ";

            if (operation_mode.Equals("program"))
            {
                if(erase)
                {
                    erase_arg = " -c";
                }
                if(verify)
                {
                    verify_arg = " --verify";
                }
                mode_arg = erase_arg + verify_arg + " -f \"" + file_path  + "\"";
            }
            else if (operation_mode.Equals("write --fuses"))
            {
                mode_arg = " -v --values C29DF7";
            }
            else
            {
                mode_arg = null;
            }

            output = change_dir_arg + programmer + " -i " + programming_mode + " -d " + target_device + " " + operation_mode + mode_arg + " & " + "PAUSE";
            System.Diagnostics.Process.Start("CMD.exe", output);
            return output;
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }

        internal static void flash()
        {
            throw new NotImplementedException();
        }
    }
}
